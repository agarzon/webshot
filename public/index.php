<?php

require '../app/bootstrap.php';
require '../app/models/Screen.php';

// Configure Slim
$app = new \Slim\Slim(array(
    'mode' => 'development',
    'debug' => true,
    'log.enabled' => true,
    'log.level' => \Slim\Log::WARN,
    'templates.path' => APP.'/views',
));

$app->get('/', function () use ($app) {
    Controller::cleanup($app);
    $app->render('home.php');
});

$app->get('/:hash', function ($hash) use ($app) {
    $screen = Screen::where('hash', $hash)->get()->first();
    if ($screen) {
        ++$screen->views;
        $screen->save();
        $app->flashNow('success', false);
        $app->flashNow('file', $app->request->getUrl().'/screens/'.$hash.'.png');
        $app->flashNow('link', $app->request->getUrl().'/'.$screen->hash);
        $app->flashNow('download', $app->request->getUrl().'/download/'.$screen->hash);
        $app->flashNow('views', (integer) $screen->views);
        $app->flashNow('website', $screen->website);
        $app->flashNow('created', $screen->created_at);
    } else {
        $app->flashNow('error', 'No results found.');
    }
    $app->render('home.php');
});

$app->get('/download/:hash', function ($hash) use ($app) {
    if (Screen::where('hash', $hash)->count()) {
        $file = dirname(__FILE__).'/screens/'.$hash.'.png';
        if (file_exists($file)) {
            $app->response->headers->set('Content-Type', 'application/stream');
            $app->response->headers->set('Cache-Control', 'private');
            $app->response->headers->set('Content-Transfer-Encoding', 'binary');
            $app->response->headers->set('Content-Length', filesize($file));
            $app->response->headers->set('Content-Disposition', 'attachment; filename="'.basename($file)).'"';
            readfile($file);
        } else {
            $app->flashNow('error', 'No file was found in the system.');
        }
    } else {
        $app->flashNow('error', 'No results found.');
    }
    $app->render('home.php');
});

$app->post('/', function () use ($app) {
    $website = $app->request->post('website');
    if (!filter_var($website, FILTER_VALIDATE_URL) === false) {
        $hash = sha1(uniqid());
        $file = $hash.'.png';
        if (Controller::makeScreen($website, $file)) {
            $screen = new Screen();
            $screen->website = $website;
            $screen->hash = $hash;
            if ($screen->save()) {
                $app->flash('success', true);
                $app->flash('file', $app->request->getUrl().'/screens/'.$file);
                $app->flash('link', $app->request->getUrl().'/'.$screen->hash);
                $app->flash('download', $app->request->getUrl().'/download/'.$screen->hash);
                $app->flash('views', (integer) $screen->views);
                $app->flash('website', $screen->website);
            } else {
                $app->flash('error', 'Error saving in DB.');
            }
        } else {
            $app->flash('error', 'Error taking the screenshot.');
        }
    } else {
        $app->flash('error', 'Invalid URL.');
    }
    $app->flashKeep();
    $app->response->redirect('/', 303);
});

$app->run();

class Controller
{
    /**
     * Execute phantomJS.
     *
     * @param string $website
     * @param string $file
     *
     * @return bool
     */
    public static function makeScreen($website, $file)
    {
        $script = dirname(__FILE__) . '/js/webshot.js';
        $file = dirname(__FILE__) . '/screens/' . $file;
        $result = trim(shell_exec(escapeshellcmd("../bin/phantomjs --ssl-protocol=any --ignore-ssl-errors=true $script $website $file ")));
        if (is_null($result) || $result != 'success') {
            //return false; // sometimes 'success' comes among multiple lines.
        }

        return true;
    }

    /**
     * Delete old screens.
     *
     * @param object $app
     */
    public static function cleanup($app)
    {
        $screens = Screen::where('created_at', '<', Carbon\Carbon::now()->subDays(KEEPDAYS)->toDateTimeString());
        if ($screens->count()) {
            foreach ($screens->get() as $screen) {
                @unlink(dirname(__FILE__).'/screens/'.$screen->hash.'.png');
            }
            $screens->delete();
        }
    }
}
