// Use: ./phantomjs webshoot.js http://www.cnn.com/ cnn.png
var page = require('webpage').create(), system = require('system');

if (system.args.length !== 3) {
	console.log('I need the URL and the filename as arguments!, example: webshoot.js http://www.cnn.com/ <file.[jpg|png|pdf]>');
	phantom.exit(1);
} else {
	var url = system.args[1];
	var filename = system.args[2];
}

page.viewportSize = { width: 1440, height: 900 };
page.open(url, function (status) {
	if (status == 'success') {
		//page.clipRect = { top: 0, left: 0, width: 1440, height: 900 };
		page.evaluate(function() {
		  document.body.bgColor = 'white';
		});
		page.render(filename);
		console.log('success');
	}
	phantom.exit();
});
