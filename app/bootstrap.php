<?php

// Start the session
set_time_limit(300);
date_default_timezone_set('America/Caracas');
session_cache_limiter(false);
session_start();

define('APP', dirname(__FILE__));

define('KEEPDAYS', 30);
// Composer autoloading
require '../vendor/autoload.php';

// Database information
//@todo Check if phantomjs is installed or use the local one

if (!file_exists(APP.'/db/database.sqlite')) {
    copy(APP.'/db/database-sample.sqlite', APP.'/db/database.sqlite');
}

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();
$capsule->addConnection([
    'driver' => 'sqlite',
    'database' => APP.'/db/database.sqlite',
    'prefix' => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

//$db = $capsule->getDatabaseManager();
//loading simple DB tables creation
//$db->statement('CREATE TABLE "webshots" ("id" INTEGER PRIMARY KEY NOT NULL , "website" VARCHAR NOT NULL , "hash" VARCHAR, "views" INTEGER DEFAULT (0), "created" DATETIME);');
