<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Webshot :: Grab a web screenshot online</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="keywords" content="online screenshot,web screenshot,web snapshot,web capture,free web capture,free web snapshot,free web screenshot" />
		<meta name="description" content="Webshot: Online webpage screenshot tool that take a full page snapshot of a website for free" />
		<link rel="icon" type="image/png" href="/img/favicon.png" />
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootswatch/3.3.4/darkly/bootstrap.min.css" >
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="/js/common.js"></script>

		<style type="text/css" media="screen">
			body {padding-top: 10px;}
			.bgwhite {background-color: #fff !important;}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="well">
				<div class="row">
					<div class="col-md-2"><a href="/" title="Webshot"><img src="/img/logo.png" alt="webshot" class="img-responsive"></a></div>
					<div class="col-lg-10">
						<h2><a href="/" title="Webshot">Webshot</a> <small>v2.0</small></h2>
						<p>Webshot: Online webpage screenshot tool that take a full page snapshot.</p>
					</div>
				</div>
			</div>

			<form method="post" action="/" class="form-inline" autocomplete="off" role="form">
				<label for="website">Web Address: </label>
				<div class="clearfix"></div>
				<input id="website" name="website" placeholder="http://" class="form-control" required="" type="url" value="" >
				<button id="submit" name="submit" class="btn btn-primary">Submit</button>
			</form>
			<hr />

			<!-- Success -->
			<?php if ($flash['success']): ?>
				<p>Copy and share this screenshot: CTRL+C <span class="label label-info">Will be stored only for <?= KEEPDAYS ?> days !</span>
					<a href="https://twitter.com/intent/tweet?text=Check%20this%20web%20screenshot%20! <?=urlencode($flash['link'])?>" target="_blank" title="Tweet"><img src="/img/Twitter.png"></a>
				</p>
				<p><input id="linkshared" class="form-control" type="text" value="<?=$flash['link']?>" readonly></p>
			<?php endif?>

			<!-- Error -->
			<?php if ($flash['error']): ?>
				<div class="alert alert-danger" role="alert">
					<?=$flash['error']?>
				</div>
			<?php endif?>

			<!-- Result -->
			<?php if (isset($flash['file'])): ?>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-framed table-hover table-condensed">
						<thead>
							<tr class="success">
								<th>URL</th>
								<th class="text-center">Visits</th>
								<th class="text-center">Created</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="<?=$flash['website']?>" title="<?=$flash['website']?>" target="_blank"><?=$flash['website']?></a>
								</td>
								<td class="text-center"><?=$flash['views']?></td>
								<td class="text-center"><?=$flash['created']?></td>
								<td class="text-center">
									<a href="<?=$flash['download']?>" title="Download">
										<button type="button" class="btn btn-warning btn-sm" aria-label="Download">
											<span class="glyphicon glyphicon-download" aria-hidden="true"></span> Download
										</button>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<img src="<?=$flash['file']?>" alt="<?=$flash['website']?>" class="img-thumbnail img-responsive bgwhite">
			<?php endif?>

			<hr />
			<footer>
				<p>Created by <a href="https://about.me/agarzon" title="Alexander Garzon" target="_blank">Alexander Garzon</a> for <a href="http://www.venehosting.com/" title="VeneHosting.com" target="_blank">VeneHosting.com</a></p>
			</footer>
		</div>
		<!-- <a href="https://github.com/you"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://camo.githubusercontent.com/652c5b9acfaddf3a9c326fa6bde407b87f7be0f4/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f6f72616e67655f6666373630302e706e67" alt="Fork me on GitHub" data-canonical-src="https://s3.amazonaws.com/github/ribbons/forkme_right_orange_ff7600.png"></a> -->
	</body>
</html>
