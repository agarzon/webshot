<?php

class Screen extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'webshots';
    protected $guarded = ['id', 'website' ,'hash'];
}
